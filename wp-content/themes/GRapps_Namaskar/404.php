<?php get_header(); ?>


<?php get_header(); ?>


    <div class="services-page">
    <header>

        <!-- ================== NAVBAR ================ -->
        <nav class="navbar navbar-default" role="navigation">



            <div class="container">

                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar top-bar"></span>
                        <span class="icon-bar middle-bar"></span>
                        <span class="icon-bar bottom-bar"></span>
                    </button>
                </div>

				<?php
				wp_nav_menu( array(
						'menu'              => 'primary',
						'theme_location'    => 'primary',
						'depth'             => 2,
						'container'         => 'div',
						'container_class'   => 'collapse navbar-collapse',
						'container_id'      => 'bs-example-navbar-collapse-1',
						'menu_class'        => 'nav navbar-nav',
						'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
						'walker'            => new wp_bootstrap_navwalker())
				);
				?>
            </div>

        </nav>


        <!-- ================== END  NAVBAR ================ -->
    </header>

<div class="wrap-404">
    <h2>404 - Page not Found</h2>

    <div class="btn-wrap btn-header-wrap">
        <a class="btn btn-default" href="<?php echo site_url(); ?>" role="button">Back to Main</a>
    </div>
</div>



<?php get_footer(); ?>