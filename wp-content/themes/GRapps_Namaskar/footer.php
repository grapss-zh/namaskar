
<footer class="footer">
    <div class="footer-text">

        <?php
        echo '';
        /*
        <aside class="fatfooter" role="complementary">
            <div class="first quarter left widget-area">
			    <?php dynamic_sidebar( 'first-footer-widget-area' ); ?>
            </div><!-- .first .widget-area -->

            <div class="second quarter widget-area">
			    <?php dynamic_sidebar( 'second-footer-widget-area' ); ?>
            </div><!-- .second .widget-area -->

            <div class="third quarter widget-area">
			    <?php dynamic_sidebar( 'third-footer-widget-area' ); ?>
            </div><!-- .third .widget-area -->

            <div class="fourth quarter right widget-area">
			    <?php dynamic_sidebar( 'fourth-footer-widget-area' ); ?>
            </div><!-- .fourth .widget-area -->
        </aside><!-- #fatfooter -->
        */

        ?>

        <aside class="fatfooter" role="complementary">
            <div class="footer-new widget-area">
			    <?php

			    $menu_name = 'primary';

			    if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $menu_name ] ) ) {
				    $menu = wp_get_nav_menu_object( $locations[ $menu_name ] );

				    $menu_items = wp_get_nav_menu_items($menu->term_id);

				    $menu_list = '<ul id="menu-' . $menu_name . '">';

				    foreach ( (array) $menu_items as $key => $menu_item ) {
					    $title = $menu_item->title;
					    $url = $menu_item->url;
					    $menu_list .= '<li><a href="' . $url . '">' . $title . '</a></li>';
				    }
				    $menu_list .= '</ul>';
			    } else {
				    $menu_list = '<ul><li>Menu "' . $menu_name . '" not defined.</li></ul>';
			    }
			    // $menu_list now ready to output
                echo $menu_list

			    ?>
            </div><!-- .footer-new -->

            <div class="social-icons-wrap">
                <?php $socialYoutube = get_field('social_youtube_link', 'option'); ?>
	            <?php $socialFacebook = get_field('social_facebook_link', 'option'); ?>
	            <?php $socialTwitter = get_field('social_twitter_link', 'option'); ?>
	            <?php $socialInstagram = get_field('social_instagram_link', 'option'); ?>

	            <?php if (!empty($socialFacebook)) { ?>
                    <a target="_blank" href="<?= $socialFacebook;?>"><img src="<?php echo get_bloginfo('template_url')?>/img/social-facebook.png" alt=""></a>
	            <?php } ?>
	            <?php if (!empty($socialInstagram)) { ?>
                    <a target="_blank" href="<?= $socialInstagram;?>"><img src="<?php echo get_bloginfo('template_url')?>/img/social-instagram.png" alt=""></a>
	            <?php } ?>
                <?php if (!empty($socialYoutube)) { ?>
                    <a target="_blank" href="<?= $socialYoutube;?>"><img src="<?php echo get_bloginfo('template_url')?>/img/social-youtube.png" alt=""></a>
                <?php } ?>
	            <?php if (!empty($socialTwitter)) { ?>
                    <a target="_blank" href="<?= $socialTwitter;?>"><img src="<?php echo get_bloginfo('template_url')?>/img/social-twitter.png" alt=""></a>
	            <?php } ?>


            </div>
        </aside><!-- #fatfooter -->


    </div>
</footer>

	<?php wp_footer(); ?>

	<!-- Don't forget analytics -->

</body>

</html>
