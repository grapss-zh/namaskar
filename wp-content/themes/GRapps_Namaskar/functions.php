<?php

    function jquery_cdn() {
        wp_deregister_script('jquery');
        wp_register_script('jquery', 'http://code.jquery.com/jquery-1.11.0.js', false, '1.11.0');



        wp_enqueue_script('jquery');
    }
    add_action('init', 'jquery_cdn');


        /* Disable the Admin Bar. */
        // remove_action( 'init', 'wp_admin_bar_init' );


    // Add RSS links to <head> section
    automatic_feed_links();

    function organizedthemes_load_default_scripts() {   
        if( !is_admin()){ 
           wp_register_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '1.1.0', true );
	        wp_register_script('flexslider', get_template_directory_uri() . '/js/jquery.flexslider.js', array('jquery'), '1.1.0', true );
	        wp_register_script('namaskar', get_template_directory_uri() . '/js/namaskar.js', array('jquery'), '1.1.0', true );

	        wp_enqueue_style( 'flexsilder_css', get_template_directory_uri() . '/css/flexslider.css', false, '1.0.0' );

        }
    }
    add_action('wp_enqueue_scripts', 'organizedthemes_load_default_scripts');

	// load css/admin.css to admin part.
	function load_admin_styles() {
		wp_enqueue_style( 'admin_css', get_template_directory_uri() . '/css/admin.css', false, '1.0.0' );
	}
	add_action( 'admin_enqueue_scripts', 'load_admin_styles' );


function organizedthemes_conditional_script_loading() {
        // Load these scripts on every page
        wp_enqueue_script('bootstrap');
		wp_enqueue_script('flexslider');
		wp_enqueue_script('namaskar');

        // // Conditional load scripts

        // // home page
        // if ( is_page_template('home.php') ) {
        //     wp_enqueue_script('lazy');
        //     wp_enqueue_script('raphael');
        //     wp_enqueue_script('svg');
        //     wp_enqueue_script('owl');
        // } 
        // // portfolio 
        // if (is_page_template('page-portfolio.php')) {
        //     wp_enqueue_script('isotope');
        //     wp_enqueue_script('portfolio');
        // }
        // // clients
        // if ( is_page_template('page-clients.php')) {
        //     wp_enqueue_script('isotope');
        //     wp_enqueue_script('clients');

        // }
  
    }
    add_action('wp_enqueue_scripts', 'organizedthemes_conditional_script_loading');



        // hide admin bar
        show_admin_bar( false );


    // Clean up the <head>
    function removeHeadLinks() {
        remove_action('wp_head', 'rsd_link');
        remove_action('wp_head', 'wlwmanifest_link');
    }
    add_action('init', 'removeHeadLinks');
    remove_action('wp_head', 'wp_generator');
    
    // Declare sidebar widget zone
    if (function_exists('register_sidebar')) {
        register_sidebar(array(
            'name' => 'Sidebar Widgets',
            'id'   => 'sidebar-widgets',
            'description'   => 'These are widgets for the sidebar.',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h2>',
            'after_title'   => '</h2>'
        ));
    }

    //require Custom Navigation Walker
    require_once('wp_bootstrap_navwalker.php');
    
    if (function_exists('register_nav_menus')) {
        register_nav_menus( array(
        'primary' => __( 'Primary Menu', 'Main Menu' ),
) );    
    }



//     if (is_admin()) {
// // Add a widget in WordPress Dashboard
// function wpc_dashboard_widget_function() {


//     $manuel = get_template_directory_uri().'/manuel-rightandleft.pdf';
//     // Entering the text between the quotes
//     echo "<ul>
//     <li>לקבלת עזרה בצעדים הראשונים, או לכל שאלה, עומד לשירותך מדריך למשתמש</li>
//     <li>קישור למדריך: <a href= '$manuel' > מדריך למשתמש - Right & Left</a></li>
//     <br/>
//     <li>במקרה ומשהו עדיין לא ברור, את מוזמנת לפנות אלינו</li>
//     <li> מייל:<a href='Mail: contactus@sidecode.co.il'> ContactUs@sidecode.co.il</a>
//     </ul>";
// }


// function wpc_add_dashboard_widgets() {
//         date_default_timezone_set('Asia/Jerusalem');

//         // 24-hour format of an hour without leading zeros
//         $Hour = date('G');
//         if ( $Hour >= 5 && $Hour <= 11 ) {
//             $greet = 'בוקר טוב';
//         } else if ( $Hour >= 12 && $Hour <= 18 ) {
//             $greet = 'צהריים טובים';
//         } else if ( $Hour >= 19 && $Hours <= 22 ) {
//             $greet = 'ערב טוב';
//         } else if ( $Hour >= 23 || $Hours <= 5) {
//             $greet = 'לך לישון';
//         }


//     wp_add_dashboard_widget('wp_dashboard_widget', $greet.' לקוח :)' , 'wpc_dashboard_widget_function');
// }
// add_action('wp_dashboard_setup', 'wpc_add_dashboard_widgets' );

// }





// ====== Tours - CPT ==============
// Register Custom Post Type
function custom_post_type_tours() {

	$labels = array(
		'name'                  => _x( 'Tours', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Tour', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Tours', 'text_domain' ),
		'name_admin_bar'        => __( 'Tour', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'View Item', 'text_domain' ),
		'view_items'            => __( 'View Items', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Tour', 'text_domain' ),
		'description'           => __( 'Post Type Description', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'custom-fields','page-attributes' ),
		'taxonomies'            => array( 'category' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'menu_icon' => 'dashicons-palmtree',
		'can_export'            => true,
		'has_archive'           => true,
		'menu_order'            => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'tours', $args );

}
add_action( 'init', 'custom_post_type_tours', 0 );
// ======== END: Tours - CPT =======

/**
 * Add theme settings page.
 */
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page( array(
		'page_title' => 'Theme Settings'
	));
}


// ===== Footer widget area =========
function grapps_widgets_init() {

	// First footer widget area, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => __( 'First Footer Widget Area', 'text_domain' ),
		'id' => 'first-footer-widget-area',
		'description' => __( 'The first footer widget area', 'tutsplus' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Second Footer Widget Area, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => __( 'Second Footer Widget Area', 'text_domain' ),
		'id' => 'second-footer-widget-area',
		'description' => __( 'The second footer widget area', 'text_domain' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Third Footer Widget Area, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => __( 'Third Footer Widget Area', 'text_domain' ),
		'id' => 'third-footer-widget-area',
		'description' => __( 'The third footer widget area', 'text_domain' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Fourth Footer Widget Area, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => __( 'Fourth Footer Widget Area', 'text_domain' ),
		'id' => 'fourth-footer-widget-area',
		'description' => __( 'The fourth footer widget area', 'text_domain' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

}

add_action( 'widgets_init', 'grapps_widgets_init' );

//
///**
// * ====================================================
// * Help Contact Form 7 Play Nice With Bootstrap
// * ====================================================
// * Add a Bootstrap-friendly class to the "Contact Form 7" form
// */
//add_filter( 'wpcf7_form_class_attr', 'wildli_custom_form_class_attr' );
//function wildli_custom_form_class_attr( $class ) {
//	$class .= ' form-horizontal';
//	return $class;
//}