<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/img/icon.png" />


    <?php if (is_search()) { ?>
	   <meta name="robots" content="noindex, nofollow" /> 
	<?php } ?>

	<title>
		   <?php
		      if (function_exists('is_tag') && is_tag()) {
		         single_tag_title("Tag Archive for &quot;"); echo '&quot; - '; }
		      elseif (is_archive()) {
		         wp_title(''); echo ' Archive - '; }
		      elseif (is_search()) {
		         echo 'Search for &quot;'.wp_specialchars($s).'&quot; - '; }
		      elseif (!(is_404()) && (is_single()) || (is_page())) {
		         wp_title(''); echo ' - '; }
		      elseif (is_404()) {
		         echo 'Not Found - '; }
		      if (is_home()) {
		         bloginfo('name'); echo ' - '; bloginfo('description'); }
		      else {
		          bloginfo('name'); }
		      if ($paged>1) {
		         echo ' - page '. $paged; }
		   ?>
	</title>
	
  <?php wp_head(); ?>
	<link href="<?php echo get_bloginfo('template_url')?>/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">







	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">


	
</head>

<body <?php body_class(); ?>>

	<div id="page-wrap">

<header>

    <div class="main-logo">
        <a href="<?php echo home_url(); ?>">
            <img src="<?php echo get_bloginfo('template_url')?>/img/logo.png" alt=""/>
        </a>
    </div>



    <div class="home-top-border"></div>
    </ul>
    <?php $images = get_field('homepage_gallery'); ?>
    <?php if( $images ): ?>
    <div class="flexslider flexslider-homepage">
        <ul class="slides">

	        <?php foreach( $images as $image ): ?>
                <li>
                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                </li>
	        <?php endforeach; ?>

        </ul>
    </div>
	<?php endif; ?>

    <div class="book-a-tour-wrap">
        <div class="book-a-tour">
            <a href="/contact">
                <div class="book-img-wrap">
                    <img src="<?php echo get_bloginfo('template_url')?>/img/book_a_tour.png" alt=""/>
                </div>
            </a>
        </div>
    </div>


<!-- ================== NAVBAR ================ -->
<nav class="navbar navbar-default" role="navigation">



    <div class="container">

        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar top-bar"></span>
            <span class="icon-bar middle-bar"></span>
            <span class="icon-bar bottom-bar"></span>
          </button>
        </div>

            <?php
                wp_nav_menu( array(
                    'menu'              => 'primary',
                    'theme_location'    => 'primary',
                    'depth'             => 2,
                    'container'         => 'div',
                    'container_class'   => 'collapse navbar-collapse',
                    'container_id'      => 'bs-example-navbar-collapse-1',
                    'menu_class'        => 'nav navbar-nav',
                    'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                    'walker'            => new wp_bootstrap_navwalker())
                );
            ?>
    </div>

</nav>


 <!-- ================== END  NAVBAR ================ -->
</header>



















