
jQuery(window).load(function() {
    jQuery('.flexslider-single').flexslider({
        animation: "fade",
        slideshow: true,
        animationLoop: true,
        controlNav: true,
        slideshowSpeed: 4000,
        animationSpeed: 1200,
        smoothHeight: true
    });
});


jQuery(window).load(function() {
    jQuery('.flexslider').flexslider({
        animation: "slide",
        slideshow: true,
        animationLoop: true,
        controlNav: false,
        slideshowSpeed: 5000,
        animationSpeed: 2000,
    });
});

