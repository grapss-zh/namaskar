
<?php
/*
  *  Template Name: Contact
  *
*/
?>

<?php get_header(); ?>


<div class="contact-page">
    <header>
        <div class="main-banner">
            <?php
            $image = get_field('banner_image');
            if( !empty($image) ): ?>
                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
            <?php endif; ?>
        </div>
        <!-- ================== NAVBAR ================ -->
        <nav class="navbar navbar-default" role="navigation">



            <div class="container">

                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar top-bar"></span>
                        <span class="icon-bar middle-bar"></span>
                        <span class="icon-bar bottom-bar"></span>
                    </button>
                </div>

                <?php
                wp_nav_menu( array(
                        'menu'              => 'primary',
                        'theme_location'    => 'primary',
                        'depth'             => 2,
                        'container'         => 'div',
                        'container_class'   => 'collapse navbar-collapse',
                        'container_id'      => 'bs-example-navbar-collapse-1',
                        'menu_class'        => 'nav navbar-nav',
                        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                        'walker'            => new wp_bootstrap_navwalker())
                );
                ?>
            </div>

        </nav>


        <!-- ================== END  NAVBAR ================ -->
    </header>




    <section class="services-wrap">
        <div class="container">
            <div class="row">

                <div class="col-sm-4">
                    <a class="phone-link" href="tel:<?php the_field('settings_phone', 'option'); ?>">
                        <div class="phone-top">
                            <img class="img-phone" src="<?php echo get_template_directory_uri() ?>/img/ic_single_phone.jpg" alt="">
                        </div>
                        <div class="phone-area">
                            <?php the_field('settings_phone', 'option'); ?>
                        </div>
                    </a>

                    <a class="phone-link" href="tel:<?php the_field('settings_mobile_phone', 'option'); ?>">
                        <div class="phone-top phone-top-2">
                            <img class="img-phone" src="<?php echo get_template_directory_uri() ?>/img/ic_single_phone_mobile.jpg" alt="">
                        </div>
                        <div class="phone-area">
                            <?php the_field('settings_mobile_phone', 'option'); ?>
                        </div>
                    </a>


                </div>


                <div class="col-sm-8">
                    <div class="contact-wrap">
                        <div class="contact-top">
                            <img class="img-email" src="<?php echo get_template_directory_uri() ?>/img/ic_single_email.jpg" alt="">
                            <a class="email-link" href="mailto:<?php the_field('settings_email', 'option'); ?>">
                                <span><?php the_field('settings_email', 'option'); ?></span>
                            </a>
                        </div>

                        <div class="contact-form-wrap">
                            <?php echo do_shortcode('[contact-form-7 id="143" title="Contact form 1"]'); ?>
                        </div>
                    </div>
                </div>










            </div><!-- END: row -->
        </div><!-- END: container -->
    </section>



    <?php get_footer(); ?>

</div>
