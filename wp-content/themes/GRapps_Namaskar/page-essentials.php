
<?php
/*
  *  Template Name: Essentials
  *
*/
?>

<?php get_header(); ?>


<div class="services-page">
    <header>
        <div class="main-banner">
            <?php
            $image = get_field('banner_image');
            if( !empty($image) ): ?>
                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
            <?php endif; ?>
        </div>
        <!-- ================== NAVBAR ================ -->
        <nav class="navbar navbar-default" role="navigation">



            <div class="container">

                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar top-bar"></span>
                        <span class="icon-bar middle-bar"></span>
                        <span class="icon-bar bottom-bar"></span>
                    </button>
                </div>

                <?php
                wp_nav_menu( array(
                        'menu'              => 'primary',
                        'theme_location'    => 'primary',
                        'depth'             => 2,
                        'container'         => 'div',
                        'container_class'   => 'collapse navbar-collapse',
                        'container_id'      => 'bs-example-navbar-collapse-1',
                        'menu_class'        => 'nav navbar-nav',
                        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                        'walker'            => new wp_bootstrap_navwalker())
                );
                ?>
            </div>

        </nav>


        <!-- ================== END  NAVBAR ================ -->
    </header>




    <section class="services-wrap">


        <div class="container-fluid">
            <div class="row">

                <div class="col-md-8">
                    <div class="content-wrap">
                        <h2><?php the_field('essentials_title'); ?></h2>
                        <div class="content">
	                        <?php the_field('essentials_content'); ?>
                        </div>
                    </div>
                </div><!-- END: col -->

                <div class="col-md-4">

                    <div class="side-wrap">
                            <div class="img-services-wrap">
				                <?php
				                $image = get_field('side_image');
				                if( !empty($image) ) { ?>

                                    <img class="img-services" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				                <?php } else { ?>
                                    <img class="img-services" src="<?php echo get_template_directory_uri() ?>/img/beach.jpg" alt="Default Tour image">
				                <?php } ?>
                            </div>
                            <div class="side-content">
                                <a href="tel:<?php the_field('settings_phone', 'option'); ?>">
                                    <div class="phone-wrap">
                                        <img class="img-phone img-phone-services" src="<?php echo get_template_directory_uri() ?>/img/ic_phone_services.jpg" alt="">
                                        <span><?php the_field('settings_phone', 'option'); ?></span>
                                    </div>
                                </a>
                                <a href="tel:<?php the_field('settings_mobile_phone', 'option'); ?>">
                                    <div class="phone-wrap">
                                        <img class="img-phone img-phone-services" src="<?php echo get_template_directory_uri() ?>/img/ic_phone_mobile_services.jpg" alt="">
                                        <span><?php the_field('settings_mobile_phone', 'option'); ?></span>
                                    </div>
                                </a>
                                <a href="mailto:<?php the_field('settings_email', 'option'); ?>">
                                    <div class="email-wrap">
                                        <img class="img-email" src="<?php echo get_template_directory_uri() ?>/img/ic_email_services.jpg" alt="">
                                        <span><?php the_field('settings_email', 'option'); ?></span>
                                    </div>
                                </a>


                            </div>
                    </div>



                </div><!-- END: col -->


            </div><!-- END: row -->
        </div><!-- END: container -->

    </section>



    <?php get_footer(); ?>

</div>
