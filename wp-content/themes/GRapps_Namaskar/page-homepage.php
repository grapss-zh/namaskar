
<?php
/*
  *  Template Name: HomePage
  *
*/
?>

<?php get_header('home'); ?>

<section class="section-website-links">
	<div class="container-fluid">
		<div class="row">
			<?php
			$image = get_field('package_tours_link');
			if( !empty($image) ): ?>
			<div class="col-sm-6 col-md-4">
				<a href="/package-tours">
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>
			</div>
			<?php endif; ?>

			<?php
			$image = get_field('customized_tours_link');
			if( !empty($image) ): ?>
                <div class="col-sm-6 col-md-4">
                    <a href="/customized-tours">
                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    </a>
                </div>
			<?php endif; ?>


			<?php
			$image = get_field('pilgrimage_tours_link');
			if( !empty($image) ): ?>
                <div class="col-sm-6 col-md-4">
                    <a href="/pilgrimage-tours">
                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    </a>
                </div>
			<?php endif; ?>

			<?php
			$image = get_field('india_in_israel_tours_link');
			if( !empty($image) ): ?>
				<div class="col-sm-6 col-md-4">
					<a href="/india-in-israel-tours">
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</a>
				</div>
			<?php endif; ?>




			<?php
			$image = get_field('services_link');
			if( !empty($image) ): ?>
				<div class="col-sm-6 col-md-4">
					<a href="/services">
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</a>
				</div>
			<?php endif; ?>

			<?php
			$image = get_field('essentials_link');
			if( !empty($image) ): ?>
				<div class="col-sm-6 col-md-4">
					<a href="/essentials">
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</a>
				</div>
			<?php endif; ?>

		</div>
	</div>
</section>



<?php get_footer(); ?>