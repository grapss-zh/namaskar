
<?php
/*
  *  Template Name: India Tours
  *
*/
?>

<?php get_header(); ?>


<div class="tours-page">
    <header>

        <div class="main-banner">
	        <?php
	        $image = get_field('banner_image');
	        if( !empty($image) ): ?>
                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
	        <?php endif; ?>
        </div>

        <div class="book-a-tour-wrap">
            <div class="book-a-tour">
                <a href="/contact">
                    <div class="book-img-wrap">
                        <img src="<?php echo get_bloginfo('template_url')?>/img/book_a_tour.png" alt=""/>
                    </div>
                </a>
            </div>
        </div>
        <!-- ================== NAVBAR ================ -->
        <nav class="navbar navbar-default" role="navigation">



            <div class="container">

                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar top-bar"></span>
                        <span class="icon-bar middle-bar"></span>
                        <span class="icon-bar bottom-bar"></span>
                    </button>
                </div>

                <?php
                wp_nav_menu( array(
                        'menu'              => 'primary',
                        'theme_location'    => 'primary',
                        'depth'             => 2,
                        'container'         => 'div',
                        'container_class'   => 'collapse navbar-collapse',
                        'container_id'      => 'bs-example-navbar-collapse-1',
                        'menu_class'        => 'nav navbar-nav',
                        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                        'walker'            => new wp_bootstrap_navwalker())
                );
                ?>
            </div>

        </nav>


        <!-- ================== END  NAVBAR ================ -->
    </header>




    <section class="tours-wrap">

	    <?php $catName =  'India In Israel Tours' ; ?>

	    <?php $catId = get_cat_ID( $catName ) ?>

	    <?php $categories = get_terms( 'category', array(
		    'child_of' => $catId,
		    'meta_key'			=> 'order',
		    'orderby'			=> 'meta_value',
		    'order'				=> 'ASC',

	    ) ); ?>

        <div class="container">
            <div class="row">
			    <?php $colorIndex = 0; ?>
			    <?php foreach ($categories as $cat) { ?>
				    <?php if( !empty($cat) ) { ?>
					    <?php
					    $tourColor =  get_field('category_color', $cat);
					    if (empty($tourColor)) {
						    $tourColor = '#7F3B44';
					    }
					    ?>
                        <div class="col-sm-4">
                            <div class="tour-wrap">
                                <div class="tour-title"><?php echo $cat->name; ?></div>
                                <div class="img-tour-wrap" style="border: 2px solid <?php echo $tourColor; ?>">
								    <?php
								    $image = get_field( 'category_images', $cat );
								    if( !empty($image) ) { ?>
                                        <img class="img-tour" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
								    <?php } else { ?>
                                        <img class="img-tour" src="<?php echo get_template_directory_uri() ?>/img/beach.jpg" alt="Default Tour image">
								    <?php } ?>
                                </div>

                                <ul class="tour-list" style="background-color: <?php echo $tourColor;?>">
								    <?php $loop = new WP_Query(
									    array(
										    'cat' => $cat->term_id,
										    'post_type' => 'tours',
										    'posts_per_page' => -1,
										    'orderby'  => 'menu_order',
										    'order'    => 'ASC',

									    )
								    ); ?>
								    <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                                        <li class="list-item"><a href="<?php the_permalink()?>"><?php echo  get_field('link_text') ?></a></li>
								    <?php endwhile; ?>
                                </ul>
                            </div>
                        </div>
				    <?php } ?>
			    <?php } ?>
            </div>
        </div>

    </section>



    <?php get_footer(); ?>

</div>
