<?php get_header(); the_post(); ?>



<?php

    $post_categories = get_the_terms( get_the_ID(), 'category');
    if( !empty($post_categories) ) {
        $post_cat   = array_pop( $post_categories );
        $parent_cat = get_category( $post_cat->parent );
    }

?>

    <header>
        <div class="main-banner">
            <?php
            $image = get_field( 'category_images', $parent_cat );
			if( !empty($image) ): ?>
                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			<?php endif; ?>
        </div>

        <div class="book-a-tour-wrap">
            <div class="book-a-tour">
                <a href="/contact">
                    <div class="book-img-wrap">
                        <img src="<?php echo get_bloginfo('template_url')?>/img/book_a_tour.png" alt=""/>
                    </div>
                </a>
            </div>
        </div>
        <!-- ================== NAVBAR ================ -->
        <nav class="navbar navbar-default" role="navigation">



            <div class="container">

                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar top-bar"></span>
                        <span class="icon-bar middle-bar"></span>
                        <span class="icon-bar bottom-bar"></span>
                    </button>
                </div>

				<?php
				wp_nav_menu( array(
						'menu'              => 'primary',
						'theme_location'    => 'primary',
						'depth'             => 2,
						'container'         => 'div',
						'container_class'   => 'collapse navbar-collapse',
						'container_id'      => 'bs-example-navbar-collapse-1',
						'menu_class'        => 'nav navbar-nav',
						'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
						'walker'            => new wp_bootstrap_navwalker())
				);
				?>
            </div>

        </nav>


        <!-- ================== END  NAVBAR ================ -->
    </header>

    <div class="single-tour-wrap">
        <section class="main-content">
            <div class="container">
                <div class="row">
	                <?php $tourColor =  get_field('category_color', $post_cat);
	                if (empty($tourColor)) {
		                $tourColor = '#7F3B44';
	                }
	                ?>
                        <style>
                            .tour-content h3 {
                                color: <?php echo $tourColor; ?>
                            }
                        </style>
                    <div class="col-sm-8">
                        <h2><?= $post_cat->name ?></h2>
                        <div class="tour-content">
                            <?php the_field('tour_content'); ?>
                        </div>
                    </div> <!-- END: col -->

                    <div class="col-sm-4">
                        <div class="side-wrap">


                            <div class="tour-wrap">
                                    <div class="img-tour-wrap" style="border: 2px solid <?php echo $tourColor; ?>">
                                        <?php
                                        $image = get_field('category_images', $post_cat);
                                        if( !empty($image) ) { ?>
                                            <img class="img-tour" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                                        <?php } else { ?>
	                                        <img class="img-tour" src="<?php echo get_template_directory_uri() ?>/img/beach.jpg" alt="Default Tour image">
                                        <?php } ?>
                                    </div>

                                <ul class="tour-list" style="background-color: <?php echo $tourColor;?>">
		                            <?php $loop = new WP_Query(
			                            array(
				                            'cat' => $post_cat->term_id,
				                            'post_type' => 'tours',
				                            'posts_per_page' => -1,
				                            'orderby'  => 'menu_order',
				                            'order'    => 'ASC',
			                            )
		                            ); ?>
		                            <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                                        <li class="list-item"><a href="<?php the_permalink()?>"><?php echo  get_field('link_text') ?></a></li>
		                            <?php endwhile; ?>
                                </ul>
                            </div>


                        </div>
                    </div> <!-- END: col -->
                </div> <!-- END: row -->


	            <?php

	            $images = get_field('tour_gallery');

	            if( $images ): ?>
                    <div id="slider" class="flexslider flexslider-single">
                        <ul class="slides">
				            <?php foreach( $images as $image ): ?>
                                <li>
                                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                                    <p><?php echo $image['caption']; ?></p>
                                </li>
				            <?php endforeach; ?>
                        </ul>
                    </div>

	            <?php endif; ?>


            </div><!-- END: container -->
        </section>

        <section class="bottom-content">








            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="booking-wrap">
                            <a class="btn btn-booking" href="/contact">Book this tour</a>
                            <div class="contact-wrap">
                                <a href="tel:<?php the_field('settings_phone', 'option'); ?>">
                                    <div class="phone-wrap">
                                        <img class="img-phone" src="<?php echo get_template_directory_uri() ?>/img/ic_single_phone.jpg" alt="">
                                        <span><?php the_field('settings_phone', 'option'); ?></span>
                                    </div>
                                </a>
                                <a href="tel:<?php the_field('settings_mobile_phone', 'option'); ?>">
                                <div class="phone-wrap phone-wrap-mobile">
                                        <img class="img-phone img-phone-services img-phone-services-mobile" src="<?php echo get_template_directory_uri() ?>/img/ic_single_phone_mobile.jpg" alt="">
                                        <span><?php the_field('settings_mobile_phone', 'option'); ?></span>
                                    </div>
                                </a>
                                <a href="mailto:<?php the_field('settings_email', 'option'); ?>">
                                    <div class="email-wrap">
                                        <img class="img-email" src="<?php echo get_template_directory_uri() ?>/img/ic_single_email.jpg" alt="">
                                        <span><?php the_field('settings_email', 'option'); ?></span>
                                    </div>
                                </a>


                            </div>
                        </div>
                    </div>


	                <?php $categories = get_terms( 'category', array(
		                'child_of' => $parent_cat->term_id,
		                'meta_key'			=> 'order',
		                'orderby'			=> 'meta_value',
		                'order'				=> 'ASC',
		                'number'            => 2,
		                'exclude'           => $post_cat->term_id
	                ) ); ?>

	                <?php foreach ($categories as $cat) { ?>
		                <?php if( !empty($cat) ) { ?>
			                <?php
                            $tourColor =  get_field('category_color', $cat);
			                if (empty($tourColor)) {
				                $tourColor = '#7F3B44';
			                }
                            ?>
                            <div class="col-sm-4">
                                <div class="tour-wrap">
                                    <div class="tour-title"><?php echo $cat->name; ?></div>
                                    <div class="img-tour-wrap" style="border: 2px solid <?php echo $tourColor; ?>">
						                <?php
						                $image = get_field( 'category_images', $cat );
						                if( !empty($image) ) { ?>
                                            <img class="img-tour" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						                <?php } else { ?>
                                            <img class="img-tour" src="<?php echo get_template_directory_uri() ?>/img/beach.jpg" alt="Default Tour image">
						                <?php } ?>
                                    </div>

                                    <ul class="tour-list" style="background-color: <?php echo $tourColor;?>">
						                <?php $loop = new WP_Query(
							                array(
								                'cat' => $cat->term_id,
								                'post_type' => 'tours',
								                'posts_per_page' => -1,
								                'orderby'  => 'menu_order',
								                'order'    => 'ASC',

							                )
						                ); ?>
						                <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                                            <li class="list-item"><a href="<?php the_permalink()?>"><?php echo  get_field('link_text') ?></a></li>
						                <?php endwhile; ?>
                                    </ul>
                                </div>
                            </div>
			                <?php
			                $colorIndex++;
			                if ($colorIndex >=3) {$colorIndex = 0; }
			                ?>

		                <?php } ?>
	                <?php } ?>





                </div> <!-- END: row -->
            </div><!-- END: container -->
        </section>


<!--    Bo    -->





    </div><!-- END: single-tour-wrap -->



<?php get_footer(); ?>